import {
    Meteor
} from 'meteor/meteor';
import '../imports/db.js';
import {
    airsensor
} from '../imports/db.js';
const fs = require('fs');
const csv = require('fast-csv');
const MongoClient = require('mongodb').MongoClient;
const multer = require('multer');
const url = 'mongodb://localhost:27017/meteor';
const dbName = 'poom';

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      //  cb(null, '/root/uploads/')
      cb(null, '/../../../../../uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
});

const upload = multer({ storage: storage });

Meteor.startup(() => {
    Meteor.methods({
        /////////////////////////////////REST API ELDER/////////////////////////////////
        lastdata() {
            var pipeline = [{
                "$project": {
                    "_id": 1,
                    "station_name": 1,
                    "latlng": 1,
                    "data": {
                        "$arrayElemAt": ["$data", -1]
                    }
                }
            }];
            var result = airsensor.aggregate(pipeline);
            return result

        },
        getrawdata(id, year, month) {
            var t = parseInt(month) + 1
            var a = ('0' + t).slice(-2)
            var start = year + "-" + month + "-01T00:00:00.000Z"
            var end = year + "-" + a + "-01T00:00:00.000Z"
            var pipeline = [{
                "$unwind": {
                    "path": "$data",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$match": {
                    "_id": id,
                    "data.timestamp": {
                        $gte: new Date(start),
                        $lte: new Date(end)
                    }
                }
            }, {
                "$project": {
                    "_id": "$arrayIndex",
                    "data": "$data"
                }
            }, { "$sort": { "data.timestamp": -1 } },
            ]
            // console.log(new Date(start), new Date(end));
            var result = airsensor.aggregate(pipeline);
            return result
        },
        chartData(id) {
            var pipeline = [{
                "$unwind": {
                    "path": "$data",
                    "includeArrayIndex": "arrayIndex",
                    "preserveNullAndEmptyArrays": false
                }
            },
            {
                "$match": {
                    "_id": id,
                    "data.timestamp": {
                        $gte: new Date(Date.now() - 12 * 60 * 60 * 1000),
                    }
                }
            },
            { "$sort": { "data.timestamp": -1 } },
            ]
            var result = airsensor.aggregate(pipeline);
            return result
        }

    })


    // code to run on server at startu
    // let aqibot = require('aqi-bot');
    // let concentrationData = [30.5, 28.8, 29.5, 30, 32.4, 31.1, 28.2, 30.7, 32.8, 32.6, 33.1, 28.5];
    // aqibot.AQICalculator.getAQIResult(aqibot.PollutantType.PM25, 172).then((result) => {
    //         console.log(result);
    //     }).catch(err => {
    //         console.log(err);
    //     })
    //ตัวอย่างข้อมูล
    //addsensor/id=0002/lat=18.0542578/lng=17.05587785/time=18-05-2562-15-30/co=1/co2=2/temp=3/rh=4/hpa=5/dush=6/pm25=7/pm10=8/
    Router.route('/xlsairquality/:id/:name/:month/:year', { // API 2
        where: 'server'
    }).get(function () {
        var month = this.params.month
        var year = this.params.year
        var sta_n = this.params.name
        var t = parseInt(month) + 1

        var a = ('0' + t).slice(-2)
        var start = year + "-" + month + "-01T00:00:00.000Z"
        var end = year + "-" + a + "-01T00:00:00.000Z"

        var data = airsensor.aggregate([{
            $unwind: {
                path: "$data",
                includeArrayIndex: "arrayIndex", // optional
                preserveNullAndEmptyArrays: false // optional
            }
        }, {
            $match: {
                "_id": this.params.id,
                "data.timestamp": {
                    $gte: new Date(start),
                    $lte: new Date(end)
                }
            }
        }, {
            $project: {
                _id: 1,
                station_name: 1,
                //timestamp: { $concat: [{ $toString: { $dayOfMonth: { date: "$data.timestamp", timezone: "Asia/Bangkok" } } }, "/", { $toString: { $month: { date: "$data.timestamp", timezone: "Asia/Bangkok" } } }, "/", { $toString: { $add: [{ $year: { date: "$data.timestamp", timezone: "Asia/Bangkok" } }, 543] } }] },
                "data.timestamp": 1,
                "data.latlng.lat": 1,
                "data.latlng.lng": 1,
                "data.aqi": 1,
                "data.pm25": 1,
                "data.pm10": 1,
                "data.temp": 1,
                "data.humi": 1,
                "data.kpa": 1,
                "data.co": 1,
                "data.co2": 1
            }
        }, { $sort: { fullcode: -1 } }]).toArray()
        var fields = [{ key: '_id', title: 'รหัสสถานนี' }, { key: 'station_name', title: 'ชื่อสถานนี' }, { key: 'data.timestamp', title: 'วันที่บันทึกข้อมูล' }, { key: 'data.aqi', title: 'AQI' }, { key: 'data.pm25', title: 'PM2.5' }, { key: 'data.pm10', title: 'PM10' }, { key: 'data.temp', title: 'อุณภูมิในอากาศ' }, { key: 'data.humi', title: 'ความชื้นสัมพัทธ์' }, { key: 'data.kpa', title: 'ความกดอากาศ' }, { key: 'data.co', title: 'คาร์บอนมอนออกไซด์' }, { key: 'data.co2', title: 'คาบอนไดออกไซด์' }];
        var title = 'ข้อมูล' + sta_n + " " + month + "-" + year;
        var newtitle = encodeURIComponent(title.replace(/(\r\n\t|\n|\r\t)/gm, ""));
        var file = Excel.export(newtitle, fields, data);
        var headers = {
            'Content-Type': 'application/vnd.openxmlformats',
            'Content-Disposition': 'attachment; filename=' + newtitle + '.xlsx'
        };

        this.response.writeHead(200, headers);
        this.response.end(file, 'binary');
    })

    Router.route('/api/uploadfile', upload.single("uploadfile"), {
        where: 'server'
    })
        .post(function (req, res) {
            var path = this.request.file.path;
            var name = this.request.file.filename;
            importCsvData2MySQL('/root/uploads/' + name);
            this.response.end('File uploaded/import successfully! file : ' + name + '');

        });


    Router.route('/addsensor/id=:id/lat=:lat/lng=:lng/time=:time/aqi=:aqi/pm25=:pm25/pm10=:pm10/temp=:temp/rh=:rh/kpa=:kpa/co=:co/co2=:co2', {
        where: 'server'
    })
        .get(function () {
            var sid = this.params.id;
            var timestamp = this.params.time != "" ? this.params.time.split('-') : "";
            var co = this.params.co ? this.params.co : "";
            var co2 = this.params.co2 ? this.params.co2 : "";
            var temp = this.params.temp ? this.params.temp : "";
            var rh = this.params.rh ? this.params.rh : "";
            var kpa = this.params.kpa ? this.params.kpa : "";
            var aqi = this.params.aqi ? this.params.aqi : "";
            var pm25 = this.params.pm25 ? this.params.pm25 : "";
            var pm10 = this.params.pm10 ? this.params.pm10 : "";
            var func = this
            var lat = this.params.lat ? this.params.lat : "";
            var lng = this.params.lng ? this.params.lng : "";
            console.log(sid);
            airsensor.update({
                _id: sid
            }, {
                    $set: {
                        "latlng": {
                            lat: lat,
                            lng: lng
                        },
                    },

                    $push: {
                        data: {
                            "timestamp": new Date(get2D(timestamp[2]) + "-" + get2D(timestamp[1]) + "-" + get2D(timestamp[0]) + "T" + get2D(timestamp[3]) + ":" + get2D(timestamp[4])),
                            "latlng": {
                                lat: lat,
                                lng: lng
                            },
                            "aqi": aqi,
                            "pm25": pm25,
                            "pm10": pm10,
                            "temp": temp,
                            "humi": rh,
                            "kpa": kpa,
                            "co": co,
                            "co2": co2,

                        },
                    }
                }, function (err, result) {
                    if (result == 0) {
                        response = {
                            "error": true,
                            "message": "ไม่พบสถานี"
                        }

                        func.response.setHeader('Content-Type', 'application/json');
                        func.response.end(JSON.stringify(response));
                    } else {
                        response = {
                            "success": true,
                            "message": "เก็บค่าสำเร็จ"
                        }

                        func.response.setHeader('Content-Type', 'application/json');
                        func.response.end(JSON.stringify(response));
                    }
                })



        });


    Router.route('/insertstation/id=:id/name=:name/lat=:lat/lng=:lng', {
        where: 'server'
    })
        .get(function () {
            var sid = this.params.id;
            var lat = this.params.lat;
            var lng = this.params.lng;
            var sname = this.params.name;
            airsensor.insert({
                "_id": sid,
                "station_name": sname,
                "latlng": {
                    "lat": lat,
                    "lng": lng
                },
                "data": [],
            })

            response = {
                "success": true,
                "message": "เพิ่มสถานีสำเร็จ."
            }
            this.response.setHeader('Content-Type', 'application/json');
            this.response.end(JSON.stringify(response));
        });

    Router.route('/updatestation/id=:id/name=:name', {
        where: 'server'
    })
        .get(function () {
            var sid = this.params.id;
            var lat = this.params.lat != "null" ? this.params.lat : "";
            var lng = this.params.lng != "null" ? this.params.lng : ""
            var sname = this.params.name != "null" ? this.params.name : ""
            var obj = {}
            obj = {
                "station_name": sname,
            }
            // if (lat != "" & lng != "" & sname == "") {
            //   obj = {
            //     "latlng": {
            //       "lat": lat,
            //       "lng": lng
            //     },
            //   }
            // } else if (lat == "" & lng == "" & sname != "") {
            //   obj = {
            //     "station_name": sname,
            //   }
            // } else {
            //   obj = {
            //     "station_name": sname,
            //     "latlng": {
            //       "lat": lat,
            //       "lng": lng
            //     },
            //   }
            // }
            airsensor.update({
                _id: sid
            }, {
                    $set: obj
                })
            response = {
                "success": true,
                "message": "อัพเดทสถานีสำเร็จ."
            }
            this.response.setHeader('Content-Type', 'application/json');
            this.response.end(JSON.stringify(response));
        });

    Router.route('/removestation/id=:id/', {
        where: 'server'
    })
        .get(function () {
            var sid = this.params.id;
            airsensor.remove({
                _id: sid
            })
            response = {
                "success": true,
                "message": "ลบสถานีสำเร็จ."
            }
            this.response.setHeader('Content-Type', 'application/json');
            this.response.end(JSON.stringify(response));
        });
});


function get2D(num) {
    if (num.toString().length < 2) // Integer of less than two digits
        return "0" + num; // Prepend a zero!
    return num.toString(); // return string for consistency
}

function importCsvData2MySQL(filePath) {
    let stream = fs.createReadStream(filePath);
    let csvData = [];
    let csvStream = csv
        .parse()
        .on("data", function (data) {
            csvData.push(data);


        })
        .on("end", function () {
            // Remove Header ROW
            csvData.shift();
            //console.log(csvData)
            for (let index = 0; index < csvData.length; index++) {

                if (csvData[index][0] === undefined) {
                    console.log('dont insert')

                } else {

                    if (csvData[index][0].length != 5) {
                        console.log('dont insert')
                    } else {
                        MongoClient.connect(url, function (err, client) {


                            const db = client.db(dbName);
                            updateDocument(db, csvData[index], function () {
                                client.close();
                            });

                        });

                        // Update document where a is 2, set b equal to 1

                    }

                }


            }

            fs.unlinkSync(filePath)
        });

    stream.pipe(csvStream);
}



const updateDocument = function (db, csvData, callback) {
    // Get the documents collection
    const collection = db.collection('airsensor');
    var timestamp = csvData[3].split('-')
    // Update document where a is 2, set b equal to 1
    collection.updateOne({
        _id: csvData[0]
    }, {
            $set: {
                "latlng": {
                    lat: csvData[1],
                    lng: csvData[2]
                },
            },

            $push: {
                data: {
                    "timestamp": new Date(timestamp[2] + "-" + timestamp[1] + "-" + timestamp[0] + "T" + timestamp[3] + ":" + timestamp[4]),
                    "latlng": {
                        lat: csvData[1],
                        lng: csvData[2],
                    },
                    "aqi": csvData[4],
                    "pm25": csvData[5],
                    "pm10": csvData[6],
                    "temp": csvData[7],
                    "humi": csvData[8],
                    "kpa": csvData[9],
                    "co": csvData[10],
                    "co2": csvData[11],

                },
            }
        }, function (err, result) {

            callback(result);
        });
}

// { 
//     "staion_id" : "ST001", 
//     "station_name" : "บ้านแม่วัง อ.แม่ริม", 
//     "latlng" : {
//         "lat" : "18.8051899", 
//         "lng" : "98.9742239"
//     }, 
//     "data" : [
//         {
//             "timestamp" : "18/05/2562 15:30", 
//             "latlng" : {
//                 "lat" : "18.8051899", 
//                 "lng" : "98.9742239"
//             }, 
//             "co" : "1", 
//             "co2" : "2", 
//             "temp" : "3", 
//             "rh" : "4", 
//             "hpa" : "5", 
//             "dush" : "6", 
//             "pm25" : "7", 
//             "pm10" : "8"
//         }
//     ]
// }


