import { Mongo } from 'meteor/mongo';


var database;
if(Meteor.isServer){
    //console.log("On collections ");
    database = new MongoInternals.RemoteCollectionDriver("mongodb://localhost:27017/meteor");
}

export const airsensor = new Mongo.Collection('airsensor',{ _driver: database });
