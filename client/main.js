import {
    Template
} from 'meteor/templating';
import {
    ReactiveVar
} from 'meteor/reactive-var';

import './main.html';
import {
    airsensor
} from '../imports/db.js';
Template.dashboard.onRendered(function () {

    aqi();
    pm25();
    pm10();
    // his()
    l_create();
    var redIcon = new L.Icon({
        iconUrl: 'img/marker-icon-2x-red.png',
        shadowUrl: 'img/marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
    var map = L.map('mapid').fitWorld();

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map);

    map.on('locationfound', onLocationFound);
    map.on('locationerror', onLocationError);
    //map.setZoom(16);

    map.locate({
        setView: true,
        maxZoom: 11
    });
    Meteor.call('lastdata', function (error, success) {
        if (error) {
            console.log('error', error);
        }
        if (success) {
           // console.log(success);
            Session.set('lastdata', success);
        }
    });
    Meteor.setInterval(function () {
        Meteor.call('lastdata', function (error, success) {
            if (error) {
                console.log('error', error);
            }
            if (success) {
               // console.log(success);
                Session.set('lastdata', success);
            }
        });
    }, 5000);
    setTimeout(function () {
        console.log(Session.get('lastdata'))
        if(Session.get('lastdata')){
            
            var datast = Session.get('lastdata')

            console.log(datast)
            var nowlatlong = Session.get('nowlatlong')
            datast.forEach(x => {
                L.marker(x.latlng, {
                    icon: redIcon,
                    name: x.station_name,
                    stationdata: x
                }).addTo(map)
                    .bindPopup("<center><b>" + x.station_name + "</b><br /> (" + x._id + ")</center>")
                    .on('click', function (e) {
                        Session.set('lastsensor', null);
                        Session.set('title', x.station_name);
    
    
                        Meteor.call('chartData', x._id, function (err, res) {
                            var aaqi = [], apm25 = [], apm10 = [], aco2 = [], aco = [], atemp = [], ahumi = [], akpa = [], atime = []
                            for (let index = 0; index < res.length; index++) {
    
                                aaqi.push(parseInt(res[index].data.aqi))
                                apm25.push(parseInt(res[index].data.pm25))
                                apm10.push(parseInt(res[index].data.pm10))
                                aco2.push(parseInt(res[index].data.co2))
                                aco.push(parseInt(res[index].data.co))
                                atemp.push(parseInt(res[index].data.temp))
                                ahumi.push(parseInt(res[index].data.humi))
                                akpa.push(parseInt(res[index].data.kpa))
                                atime.push(presenttime(res[index].data.timestamp))
    
                            }
    
                            Session.set('chart12HoursAQI', aaqi)
                            Session.set('chart12HoursPM25', apm25)
                            Session.set('chart12HoursPM10', apm10)
                            Session.set('chart12HoursCO2', aco2)
                            Session.set('chart12HoursCO', aco)
                            Session.set('chart12HoursTEMP', atemp)
                            Session.set('chart12HoursHUMI', ahumi)
                            Session.set('chart12HoursKPA', akpa)
                            Session.set('chart12HoursTIME', atime)
                            his(aaqi, atime, 'AQI', '', '')
                            $('#aqi').removeClass("w3-teal").addClass("w3-grey");
    
                        })
                        var dis = getDistanceFromLatLonInKm(nowlatlong.lat, nowlatlong.lng, e.latlng.lat, e.latlng.lng)
                        Session.set('distance', dis);
                        if (e.sourceTarget.options.stationdata.data) {
                            Session.set('lastsensor', e.sourceTarget.options.stationdata.data);
                        } else {
                            Session.set('lastsensor', null);
                        }
    
                    });
            });
    
            var near = {}
            for (let index = 0; index < datast.length; index++) {
                const data = datast[index];
    
                var distance = getDistanceFromLatLonInKm(nowlatlong.lat, nowlatlong.lng, data.latlng.lat, data.latlng.lng)
                if (!near.distance) {
                    near = data
                    near.distance = distance;
                }
    
                if (distance < near.distance) {
                    near = data
                    near.distance = distance;
                }
    
            }
            Session.set('title', near.station_name)
            Session.set('distance', near.distance)
            Session.set('id', near._id)
            if (near.data) {
                Session.set('lastsensor', near.data);
            } else {
                Session.set('lastsensor', null);
            }
        }
       
        l_destroy();

    }, 4000);

    function onLocationFound(e) {
        var radius = e.accuracy / 2;
        Session.set('nowlatlong', e.latlng)
        L.marker(e.latlng).addTo(map).bindPopup("คุณอยู่ที่นี่").openPopup();
        L.circle(e.latlng, radius).addTo(map)
    }

    function onLocationError(e) {
        Session.set('nowlatlong', {
            lat: '18.7879248',
            lng: '98.9852407'
        })
        L.marker({
            lat: '18.7879248',
            lng: '98.9852407'
        }).addTo(map).bindPopup("คุณอยู่ที่นี่").openPopup();
        map.setView([18.7879248, 98.9852407], 11);
        var datast = Session.get('lastdata')
        var nowlatlong = Session.get('nowlatlong')
        datast.forEach(x => {
            L.marker(x.latlng, {
                icon: redIcon,
                name: x.station_name,
                stationdata: x
            }).addTo(map)
                .bindPopup("<center><b>" + x.station_name + "</b><br /> (" + x._id + ")</center>")
                .on('click', function (e) {
                    Session.set('title', x.station_name);
                    var dis = getDistanceFromLatLonInKm(nowlatlong.lat, nowlatlong.lng, e.latlng.lat, e.latlng.lng)
                    Session.set('distance', dis);
                    if (e.sourceTarget.options.stationdata.data) {
                        Session.set('lastsensor', e.sourceTarget.options.stationdata.data);
                    } else {
                        Session.set('lastsensor', null);
                    }

                });
        });

        var near = {}
        for (let index = 0; index < datast.length; index++) {
            const data = datast[index];

            var distance = getDistanceFromLatLonInKm(nowlatlong.lat, nowlatlong.lng, data.latlng.lat, data.latlng.lng)
            if (!near.distance) {
                near = data
                near.distance = distance;
            }

            if (distance < near.distance) {
                near = data
                near.distance = distance;
            }

        }
        Session.set('title', near.station_name)
        Session.set('distance', near.distance)
        Session.set('id', near._id)



        if (near.data) {
            Session.set('lastsensor', near.data);
        } else {
            Session.set('lastsensor', null);
        }
    }




    setTimeout(function () {
        map.closePopup();
    }, 5000)


});

Template.dashboard.helpers({
    title: function () {
        return Session.get('title');

    },
    distance() {
        return Session.get('distance');

    },
    id() {
        return Session.get('id');

    },
    lastsensor() {
        return Session.get('lastsensor')
    },
    dateago() {
        if (Session.get('lastsensor')) {
            return prettyDate(Session.get('lastsensor').timestamp)
        } else {
            return "-"
        }

    },
    lastdata() {
        return Session.get('lastdata')
    },
    history() {
        return Session.get('history')
    }
});

Template.dashboard.events({
    'click #dexcel': function (event, template) {
       // console.log('ssss')
        if ($('#staion').val() && $('#year').val() && $('#mouth').val()) {
            var stat_id = $('#staion').val().split('-')[0]
            var stat_n = $('#staion').val().split('-')[1]
            window.location.assign('https://aqi.mju.ac.th/xlsairquality/' + stat_id + '/' + stat_n + '/' + $('#mouth').val() + '/' + $('#year').val());
        } else {
            alert('กรุณาเลือกข้อมูลให้ครบก่อนทำการดาวน์โหลด')
        }

    },
    'click #find': function (event, template) {
        if ($('#staion').val()) {
            var station = $('#staion').val().split('-')[0]
        //  console.log(station, $('#year').val(), $('#mouth').val());
            Meteor.call('getrawdata', station, $('#year').val(), $('#mouth').val(), function (error, success) {
                if (error) {
                    console.log('error', error);
                }
                if (success) {
                    console.log(success);
                    Session.set('history', success)
                }
            });
        } else {
            alert('กรุณาเลือกสถานนี')
        }
    },
    'click #kpa'() {
        $('#kpa').removeClass("w3-teal").addClass("w3-grey");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");

        his(Session.get('chart12HoursKPA'), Session.get('chart12HoursTIME'), 'KPA', '', '#4682B4')
    },
    'click #aqi'() {
        $('#aqi').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");

        his(Session.get('chart12HoursAQI'), Session.get('chart12HoursTIME'), 'AQI', '', '')
    },
    'click #pm25'() {

        $('#pm25').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");


        his(Session.get('chart12HoursPM25'), Session.get('chart12HoursTIME'), 'PM2.5', '', '#FFA07A')
    },
    'click #pm10'() {
        $('#pm10').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");

        his(Session.get('chart12HoursPM10'), Session.get('chart12HoursTIME'), 'PM10', '', '#A9A9A9')
    },
    'click #co2'() {
        $('#co2').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");


        his(Session.get('chart12HoursCO2'), Session.get('chart12HoursTIME'), 'CO2', '', '#48D1CC')
    },
    'click #co'() {
        $('#co').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");

        his(Session.get('chart12HoursCO'), Session.get('chart12HoursTIME'), 'CO', '', '#5F9EA0')
    },
    'click #temp'() {
        $('#temp').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#humi').removeClass("w3-grey").addClass("w3-teal");


        his(Session.get('chart12HoursTEMP'), Session.get('chart12HoursTIME'), 'Temperature', '', '#FFA500')
    },
    'click #humi'() {
        $('#humi').removeClass("w3-teal").addClass("w3-grey");
        $('#kpa').removeClass("w3-grey").addClass("w3-teal");
        $('#aqi').removeClass("w3-grey").addClass("w3-teal");
        $('#pm25').removeClass("w3-grey").addClass("w3-teal");
        $('#pm10').removeClass("w3-grey").addClass("w3-teal");
        $('#co2').removeClass("w3-grey").addClass("w3-teal");
        $('#co').removeClass("w3-grey").addClass("w3-teal");
        $('#temp').removeClass("w3-grey").addClass("w3-teal");

        his(Session.get('chart12HoursHUMI'), Session.get('chart12HoursTIME'), 'Humidity', '', '#6495ED')
    },
});

function aqi() {
    Highcharts.chart('aqichart', {

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: '100%'

        },

        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#000'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {}, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }],

        },

        yAxis: {
            min: 0,
            max: 400,
            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 1,
            },
            title: {
                text: 'AQI',
                style: {
                    "color": "#333333",
                    "fontSize": "16px",

                },
                y: 30
            },
            plotBands: [{
                from: 0,
                to: 50,
                color: '#55b947',

            }, {
                from: 50,
                to: 100,
                color: '#f6eb14'
            }, {
                from: 100,
                to: 150,
                color: '#f47e20'
            }, {
                from: 150,
                to: 200,
                color: '#ed1f24'
            }, {
                from: 200,
                to: 300,
                color: '#971b4d'
            }, {
                from: 300,
                to: 400,
                color: '#7d1226'
            }]
        },

        series: [{
            name: '',
            data: [Session.get('lastsensor') ? parseInt(Session.get('lastsensor')) : 0],
            tooltip: {
                valueSuffix: ''
            },
            dataLabels: {
                enabled: true,
                style: {
                    //fontWeight:'bold',
                    fontSize: '16px'
                }
            }
        }]

    },
        function (chart) {
            if (!chart.renderer.forExport) {
                setInterval(function () {
                    var point = chart.series[0].points[0]
                    if (Session.get('lastsensor')) {
                        point.update(parseInt(Session.get('lastsensor').aqi ? Session.get('lastsensor').aqi : 0));
                    } else {
                        point.update(0);
                    }
                }, 3000);
            }
        });
}

function pm25() {
    Highcharts.chart('pm25chart', {

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: '100%'
        },

        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#000'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {}, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        yAxis: {
            min: 0,
            max: 400,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 1,
            },
            title: {
                text: 'PM 2.5',
                style: {
                    "color": "#333333",
                    "fontSize": "16px",


                },
                y: 30
            },
            plotBands: [{
                from: 0,
                to: 15,
                color: '#55b947',

            }, {
                from: 15,
                to: 35,
                color: '#f6eb14'
            }, {
                from: 35,
                to: 55,
                color: '#f47e20'
            }, {
                from: 55,
                to: 150,
                color: '#ed1f24'
            }, {
                from: 150,
                to: 230,
                color: '#971b4d'
            }, {
                from: 230,
                to: 400,
                color: '#7d1226'
            }]
        },

        series: [{
            name: '',
            data: [Session.get('lastsensor') ? parseInt(Session.get('lastsensor')) : 0],
            tooltip: {
                valueSuffix: ''
            },
            dataLabels: {
                enabled: true,
                style: {
                    //fontWeight:'bold',
                    fontSize: '16px'
                }
            }
        }]

    },
        function (chart) {
            if (!chart.renderer.forExport) {
                setInterval(function () {
                    var point = chart.series[0].points[0]
                    if (Session.get('lastsensor')) {
                        point.update(parseInt(Session.get('lastsensor').pm25 ? Session.get('lastsensor').pm25 : 0));
                    } else {
                        point.update(0);
                    }
                }, 3000);
            }
        });
}

function pm10() {
    Highcharts.chart('pm10chart', {

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: '100%'
        },

        title: {
            text: ''
        },
        credits: {
            enabled: false
        },
        pane: {
            startAngle: -150,
            endAngle: 150,
            background: [{
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#000'],
                        [1, '#333']
                    ]
                },
                borderWidth: 0,
                outerRadius: '109%'
            }, {
                backgroundColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [
                        [0, '#333'],
                        [1, '#FFF']
                    ]
                },
                borderWidth: 1,
                outerRadius: '107%'
            }, {}, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
            }]
        },

        yAxis: {
            min: 0,
            max: 400,

            minorTickInterval: 'auto',
            minorTickWidth: 1,
            minorTickLength: 10,
            minorTickPosition: 'inside',
            minorTickColor: '#666',

            tickPixelInterval: 30,
            tickWidth: 2,
            tickPosition: 'inside',
            tickLength: 10,
            tickColor: '#666',
            labels: {
                step: 1,
            },
            title: {
                text: 'PM 10',
                style: {
                    "color": "#333333",
                    "fontSize": "16px",

                },
                y: 30
            },
            plotBands: [{
                from: 0,
                to: 55,
                color: '#55b947',

            }, {
                from: 55,
                to: 155,
                color: '#f6eb14'
            }, {
                from: 155,
                to: 255,
                color: '#f47e20'
            }, {
                from: 255,
                to: 245,
                color: '#ed1f24'
            }, {
                from: 245,
                to: 355,
                color: '#971b4d'
            }, {
                from: 355,
                to: 400,
                color: '#7d1226'
            }]
        },

        series: [{
            name: '',
            data: [Session.get('lastsensor') ? parseInt(Session.get('lastsensor')) : 0],
            tooltip: {
                valueSuffix: ''

            },
            dataLabels: {
                enabled: true,
                style: {
                    //fontWeight:'bold',
                    fontSize: '16px'
                }
            }
        }]

    },
        function (chart) {
            if (!chart.renderer.forExport) {
                setInterval(function () {
                    var point = chart.series[0].points[0]
                    if (Session.get('lastsensor')) {
                        point.update(parseInt(Session.get('lastsensor').pm10 ? Session.get('lastsensor').pm10 : 0));
                    } else {
                        point.update(0);
                    }
                }, 3000);
            }
        });
}

function his(cdata, ctime, ctext, unit, chartcolor, isAqi) {
    if (ctext == 'AQI') {
        Highcharts.chart('his', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ข้อมูลย้อนหลัง 12 ชั่วโมง'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ctime,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ctext + " " + unit
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} ' + unit + '</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: ctext,
                data: cdata,
                zones: [
                    {
                        value: 51,
                        color: '#55b947'
                    },
                    {
                        value: 101,
                        color: '#f6eb14'
                    },
                    {
                        value: 151,
                        color: '#f47e20'
                    },
                    {
                        value: 201,
                        color: '#ed1f24'
                    },
                    {
                        value: 301,
                        color: '#971b4d'
                    },
                    {
                        color: '#7d1226'
                    }
                ]
                // color: chartcolor

            }]

        });
    } else if (ctext == 'PM2.5') {
        Highcharts.chart('his', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ข้อมูลย้อนหลัง 12 ชั่วโมง'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ctime,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ctext + " " + unit
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} ' + unit + '</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: ctext,
                data: cdata,
                zones: [
                    {
                        value: 16,
                        color: '#55b947'
                    },
                    {
                        value: 36,
                        color: '#f6eb14'
                    },
                    {
                        value: 51,
                        color: '#f47e20'
                    },
                    {
                        value: 156,
                        color: '#ed1f24'
                    },
                    {
                        value: 236,
                        color: '#971b4d'
                    },
                    {
                        color: '#7d1226'
                    }
                ]
                // color: chartcolor

            }]

        });

    } else if (ctext == 'PM10') {
        Highcharts.chart('his', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ข้อมูลย้อนหลัง 12 ชั่วโมง'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ctime,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ctext + " " + unit
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} ' + unit + '</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: ctext,
                data: cdata,
                zones: [
                    {
                        value: 56,
                        color: '#55b947'
                    },
                    {
                        value: 161,
                        color: '#f6eb14'
                    },
                    {
                        value: 246,
                        color: '#f47e20'
                    },
                    // {
                    //     value: 156,
                    //     color: '#ed1f24'
                    // },
                    {
                        value: 356,
                        color: '#971b4d'
                    },
                    {
                        color: '#7d1226'
                    }
                ]
                // color: chartcolor

            }]

        });


    } else {
        Highcharts.chart('his', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'ข้อมูลย้อนหลัง 12 ชั่วโมง'
            },
            credits: {
                enabled: false
            },
            xAxis: {
                categories: ctime,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: ctext + " " + unit
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} ' + unit + '</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: ctext,
                data: cdata,
                color: chartcolor

            }]
            // }, function (chart) {
            //     if (!chart.renderer.forExport) {
            //         setInterval(function () {
            //             chart.showLoading();
            //             chart.update({
            //                 title: {
            //                     text: 'Monthly Average ddddddRainfall'
            //                 }
            //             })
            //             // chart.setTitle({ text: "New Title" });
            //             chart.hideLoading();
            //         }, 3000);
            //     }
        });
    }
}

function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    d = parseFloat(d).toFixed(2);
    d = parseFloat(d)
    return d;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}

function prettyDate(d) {
    var now = new Date()
    var date = new Date(d)
   // date.setTime(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
    var diff = (((now || (new Date())).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);

    if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
        return this.format(date, 'yyyy-mm-dd');

    return day_diff == 0 && (
        diff < 60 && "ตอนนี้" ||
        diff < 120 && "1 นาทีที่แล้ว" ||
        diff < 3600 && Math.floor(diff / 60) + " นาทีที่แล้ว" ||
        diff < 7200 && "1 ชั่วโมงที่แล้ว" ||
        diff < 86400 && Math.floor(diff / 3600) + " ชั่วโมงที่แล้ว") ||
        day_diff == 1 && "เมื่อวาน" ||
        day_diff < 7 && day_diff + " วันที่แล้ว" ||
        day_diff < 31 && Math.ceil(day_diff / 7) + " สัปดาห์ที่แล้ว";
}
function presenttime(d) {
    if (d) {
        var data = new Date(d)
        //data.setTime(data.getTime() + data.getTimezoneOffset() * 60 * 1000);
        var dm = moment(data).format('DD/MM/');
        var y = (parseInt(moment(data).format('YYYY')) + 543) - 2500;
        var h = moment(data).format(' HH:mm');
        var date = dm + y + h + ' น.';
        return date
    } else {
        return "-"
    }
}

l_create = function () {
    var elem = $(' <div class="preloader-background"><div class="loader" id="loader"></div></div>')
    $('body').prepend(elem);
}
l_destroy = function () {
    document.getElementById("loader").style.display = "none";
    $('.preloader-background').fadeOut('slow');
    // $('.preloader-wrapper').fadeOut();
}

/*

{
    "staion_id" : "ST001",
    "station_name" : "บ้านแม่วัง อ.แม่ริม",
    "latlng":{
        "lat": "18.8051899",
        "lng": "98.9742239"
    },
    "data":[{
        "timestamp" : new Date(),
        "aqi" : 5000,
        "pm25" : 4000,
        "pm10" : 25.5,
        "temp" : 68,
        "humi": 1500,
        "kpa" :2000,
        "co2" : 100,
        "co" : 120,
    },
    {
        "timestamp" : new Date(),
        "co" : 5000,
        "co2" : 4000,
        "temp" : 25.5,
        "rh" : 68,
        "hpa": 1500,
        "dush" :2000,
        "pm25" : 100,
        "pm10" : 120,
    }],
}

 */
/* Template.hello.helpers({
    counter() {
        return Template.instance().counter.get();
    },
});

Template.hello.events({
    'click button' (event, instance) {
        // increment the counter when button is clicked
        instance.counter.set(instance.counter.get() + 1);
    },
}); */


/* รายละเอียด app
-แสดงปริมาณฝุ่น
-แสดงค่า AQI และสีสถานะ
- แจ้งเตือนว่าควรใส่หน้ากาก
- แจ้งเตือนเมื่อค่่าขึ้นสีส้ม สีแดง
-แสดงตำแหน่ง อุปกรณ์ตาม GPS โดยเลือกแสดงตัวที่ใกล้
-แสดงข้อมูลแต่ละ ชม ภายใน 1 วัน
-แสดงข้อมูลย้อนหลัง 7 วัน 1 เดือน รวมถึีง 1 ปี

รายละเอียด dash board บน website
-เหมือนใน app
--แสดงข้อมูลย้อนหลัง 7 วัน 1 เดือน รวมถึีง 1 ปี ละเอียดกว่า app
- export data  */