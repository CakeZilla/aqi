Template.registerHelper('isnull', function (data) {
    if (data) {
        return data
    } else {
        return "-"
    }
});

Template.registerHelper('formatdatetime', function (d) {

    if (d) {
        var data = new Date(d)
        //data.setTime(data.getTime() + data.getTimezoneOffset() * 60 * 1000);
        var dm = moment(data).format('DD/MM/');
        var y = (parseInt(moment(data).format('YYYY')) + 543) - 2500;
        var h = moment(data).format(' HH:mm');
        var date = dm + y + h + ' น.';
        return date
    } else {
        return "-"
    }
});

Template.registerHelper('picaqi', function (data) {
    // value: 51,
    //                     color: '#55b947'
    //                 },
    //                 {
    //                     value: 101,
    //                     color: '#f6eb14'
    //                 },
    //                 {
    //                     value: 151,
    //                     color: '#f47e20'
    //                 },
    //                 {
    //                     value: 201,
    //                     color: '#ed1f24'
    //                 },
    //                 {
    //                     value: 301,

    var cdata = parseInt(data)
    if (data) {
        if(cdata > 301){
            return "6"
        }else if(cdata < 51){
            return "1"
        }else if(cdata < 101){
            return "2"
        }else if(cdata < 151){
            return "3"
        }else if(cdata < 201){
            return "4"
        }else if(cdata < 301){
            return "5"
        }
        //return data
    } else {
        return "7"
    }
});